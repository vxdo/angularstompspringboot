import * as SockJS from 'sockjs-client';
import {AppComponent} from './app.component';
import {Stomp} from '@stomp/stompjs';
import sizeof from 'object-sizeof';

export class WebSocketAPI {
  token = '53cb0bfa-8298-4f7d-b526-50e44270856e';
  webSocketEndPoint = 'ws://dev.api.inspirahub.id/socket/chatting/websocket?access_token=' + this.token;
  topic = '/topic/greetings';
  stompClient: any;
  appComponent: AppComponent;
  productId = 2061;
  questionId = '6d527210-d632-11ea-a16e-dd8355e0f1ce';

  constructor(appComponent: AppComponent) {
    this.appComponent = appComponent;
  }

  _connect(): void {
    console.log('Initialize WebSocket Connection');
    // const ws = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.client(this.webSocketEndPoint);
    this.stompClient.connect({}, (f) => {
      console.log(f);
    });
  }

  sub(): void {
    this.stompClient.subscribe('/user/error', this.onMessageReceived);
    this.stompClient.subscribe('/topic/products/2061/interactions', this.onMessageReceived);
  }

  _disconnect(): void {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

  // on error, schedule a reconnection attempt
  errorCallBack(error): void {
    console.log('errorCallBack -> ' + error);
    setTimeout(() => {
      this._connect();
    }, 5000);
  }

  _send(message): void {
    console.log('calling logout api via web socket: ');
    // tslint:disable-next-line:max-line-length
    this.stompClient.send('/app/products/' + this.productId + '/interactions/questions/' + this.questionId + '/answer', {}, JSON.stringify([1]));
  }

  onMessageReceived(message): void {
    console.log('size :: ' + sizeof(JSON.stringify(message.body)));

    console.log('obj :: ' + message.body);
  }
}
