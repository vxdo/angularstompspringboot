import {Component, OnInit} from '@angular/core';
import {WebSocketAPI} from './WebSocketAPI';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular8-springboot-websocket';

  webSocketAPI: WebSocketAPI;
  greeting: any;
  name: string;

  ngOnInit(): void {
    this.webSocketAPI = new WebSocketAPI(new AppComponent());
  }

  connect(): void {
    this.webSocketAPI._connect();
  }

  sub(): void {
    this.webSocketAPI.sub();
  }

  disconnect(): void {
    this.webSocketAPI._disconnect();
  }

  sendMessage(): void {
    this.webSocketAPI._send(this.name);
  }

  handleMessage(message): void {
    this.greeting = message;
  }
}
